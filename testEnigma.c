#include "enigma.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#define TEST_ROTOR "testRotor.txt"
#define MARK printf("MARKED\n");
int main(int argc, char const *argv[])
{
	int rotorId[] = {0,0,4};
	int turn[] = {13,5,22};
	char board[] = "abcdefghijklmnopqrst";
	
	Enigma e = newMachine(rotorId,turn,board,TEST_ROTOR);
	char str[27] = "abcdefghijklmnopqrstuvwxyz";
	char new[27],decoded[27];

	encode(e,str,new);
	resetMachine(e);
	encode(e,new,decoded);
	printf("PLAIN <%s>\nCODE  <%s>\nDECODE<%s>\n",str,new,decoded);
	
	destroyMachine(e);
	return 0;
}