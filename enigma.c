/*
 *Jabez Wilson(5027406)
 *Enigma Encoder (Nazi Enigma)
 *
 *current version cannot accept any character other than A-Z or a-z
 *modifications can be made to incorporate numbers as well
 */

#include "enigma.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#define MARK printf("MARKED\n");
#define LOWER_UPPER_OFFSET 32

//function declaration
	//returns the number equivalent of c
	// A,a : 0
	// Z,z : 25
	//done to include numbers in a newer version
	static char toLower(char c);
	static int numberVal(char c);
	static char charVal(int i);
	static void removeExtras(char * str);

	//initialize
	static rotor initRotor(int rotorID,int turnPos,char *fileName);
	static switchBoard initBoard(char *board);

//static function definition
	static rotor initRotor(int rotorID,int turnPos,char *fileName){
		rotor ret;
		ret.rotorID = rotorID;
		ret.turn = turnPos;
		ret.origTurn = turnPos;
		int i=0;
		FILE *f = fopen(fileName,"r");
		char buf[NUM_ELEMS+2];
		i=0;
		fgets(buf,NUM_ELEMS+2,f);
		buf[NUM_ELEMS] = '\0';
		while(i<rotorID){
			fgets(buf,NUM_ELEMS+2,f);
			buf[NUM_ELEMS] = '\0';
			i++;
		}

		i=0;
		while(buf[i] != '\0'){
			ret.forward[i] = buf[i];
			ret.reverse[numberVal(buf[i])] = charVal(i);
			
			i++;
		}


		fclose(f);
		return ret;
	}
	static char toLower(char c){
		char ret;
		assert((c >= 'a' && c <= 'z')||(c >= 'A' && c <= 'Z'));
		if(c >= 'a' && c <= 'z')ret = c;
		else ret = c + LOWER_UPPER_OFFSET;

		return ret;
	}

	static int numberVal(char c){
		return toLower(c) - 'a';
	}

	static char charVal(int i){
		return i + 'a';
	}

	static void removeExtras(char *str){
		char *p = NULL,*cursor = NULL;
		cursor = str;
		while(*cursor != '\0'){
			if(*cursor == ' ' || *cursor == '\n'){
				p = cursor;
				while(p!=NULL){
					*p = *(p+1);
					p++;
				}
			}
			cursor++;

		}
	}

	static switchBoard initBoard(char * board){ 
		switchBoard ret;
		char c;

		char brd[30];
		strcpy(brd,board);
		removeExtras(brd);
		strcpy(board,brd);
		assert(strlen(board) == 2*NUM_BOARD_PAIRS);

		int i =0;
		while(i<NUM_ELEMS){
			ret.transfer[i] = charVal(i);
			i++;
		}

		i=0;
		while(board[i] != '\0'){
			if(i%2 == 0)c = board[i+1];
			else c = board[i-1];

			ret.transfer[numberVal(board[i])] = c;
			i++; 
		}

		return ret;
	}

//functions
	Enigma newMachine(int rotorID[],int turnPos[],char* board,char *file){
		Enigma ret = malloc(sizeof(struct _enigma));
		int i =0;
		while(i<NUM_ROTORS){
			
			ret->aux[i] = initRotor(rotorID[i],turnPos[i],file);
			i++;
		}
		ret->board = initBoard(board);

		return ret;
	}
	void destroyMachine(Enigma e){
		free(e);
	}

	char passRotors(Enigma e,char pass){
		int i =0;
		int elem;
		char tmp;
		while(i<NUM_ROTORS-1){
			elem = (numberVal(pass)+e->aux[i].turn)%NUM_ELEMS;
			tmp = e->aux[i].forward[elem];
			pass = tmp;
			i++;
		}
			//reflector
			tmp = e->aux[REFLECTOR].forward[numberVal(pass)];
			pass = tmp;
		i = NUM_ROTORS-2;
		while(i>=0){
			elem = (numberVal(pass))%NUM_ELEMS;
			// elem = (numberVal(pass)-e->aux[i].turn);
			// if(elem < 0)elem += NUM_ELEMS;
			elem = numberVal(e->aux[i].reverse[elem]);
			elem = (elem-e->aux[i].turn);
			if(elem <0)elem += NUM_ELEMS;
			pass = charVal(elem);
			i--;
		}

		i=0;
		while(e->aux[i].turn == NUM_ELEMS-2 && i < NUM_ROTORS-1){
			e->aux[i].turn = 0;
			i++;
		}
		if(i<NUM_ROTORS){
			e->aux[i].turn++;
		}

		return pass;
	}

	char passBoard(Enigma e,char pass){
		return e->board.transfer[numberVal(pass)];
	}

	char passMachine(Enigma e,char pass){
		char ret = pass;
		ret = passBoard(e,ret);
		ret = passRotors(e,ret);
		ret = passBoard(e,ret);

		return ret;
	}

	void encode(Enigma e,char* text,char* output){
		int i = 0;
		char c;
		while(text[i] != '\0'){
			c = text[i];
			if((c >= 'a' && c <= 'z')||(c >= 'A' && c <= 'Z')){
				output[i] = passMachine(e,text[i]);
			}
			else {
				output[i] = text[i];
			}
			i++;
		}
		output[i] = '\0';
	}

	void resetMachine(Enigma e){
		int i =0;
		while(i<NUM_ROTORS){
			e->aux[i].turn = e->aux[i].origTurn;
			i++;
		}
	}
