#define NUM_ELEMS 26
#define NUM_BOARD_PAIRS 10
#define NUM_ROTORS 3
#define REFLECTOR (NUM_ROTORS-1)

//ID 0-3 normal rotors
//ID 4   reflector

//#define ROTOR_FILE "rotors.txt"

//Struct
	typedef struct _rotor{
		int rotorID;
		char forward[NUM_ELEMS];
		char reverse[NUM_ELEMS];
		int turn,origTurn;
	}rotor;

	typedef struct _switchBoard{
		char transfer[NUM_ELEMS];
	}switchBoard;

	typedef struct _enigma{
		rotor aux[NUM_ROTORS];
		switchBoard board;
	}enigma;

	typedef struct _enigma* Enigma;

//functions
	Enigma newMachine(int rotorID[],int turnPos[],char* board,char *file);
	void destroyMachine(Enigma e);	
	char passRotors(Enigma e,char pass);
	char passBoard(Enigma e,char pass);
	char passMachine(Enigma e,char pass);

	void resetMachine(Enigma e);

	void encode(Enigma e,char* text,char* output);

