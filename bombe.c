#include "enigma.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <time.h>

#define TEST_ROTOR "testRotor.txt"
#define EMPTY_BOARD "aabbccddeeffgghhiijj"
#define LOWER_UPPER_OFFSET 32

#define MARK printf("<<<<MARKED>>>>\n");

//MACROS for pairs matrix
	#define UNSET 0
	#define SET 1
	#define ELIMINATED 2
	#define ILLEGAL 3
	#define LEGAL 4

	#define TRUE 1
	#define FALSE 0
typedef struct _machine{
	int turn[NUM_ROTORS];
	int rotorID[NUM_ROTORS];
	switchBoard tester;
	int pairs[NUM_ELEMS][NUM_ELEMS];
}machine;

static machine solver(char* codeText,char* plainText);
static void printMachine(machine* m);
//declaration
	static char toLower(char c);

	static int numberVal(char c);

	static char charVal(int i);

	static void insertPair(machine *m, char pair[]);

	static int checkPair(int pairs[NUM_ELEMS][NUM_ELEMS], char pair[]);

	static void reinitPairs(int pairs[NUM_ELEMS][NUM_ELEMS]);

	static void eliminate(int pairs[NUM_ELEMS][NUM_ELEMS]);

	static void transferBoard(switchBoard b, char ret[]);

	static Enigma newTestMachine(machine* m,char* file);

	static void nextMachine(machine *m);	

//main
	int main(int argc, char const *argv[])
	{
		char code[] = "bzdafchejglinkpmrotqvwxyzb";
		char plain[] = "abcdefghijklmnopqrstuvwxyz";
		machine m = solver(code,plain);
		printf("ID   : <%d><%d><%d>\n",m.rotorID[0],m.rotorID[1],m.rotorID[2]);
		printf("TURN : <%d><%d><%d>\n",m.turn[0],m.turn[1],m.turn[2]);
		char str[21];
		transferBoard(m.tester,str);
		printf("BOARD: <%s>\n",str);
		return 0;
	}

//functions
	static char toLower(char c){
		char ret;
		if(!((c >= 'a' && c <= 'z')||(c >= 'A' && c <= 'Z'))){
			printf("<<%c>>\n",c);
		}
		assert((c >= 'a' && c <= 'z')||(c >= 'A' && c <= 'Z'));
		if(c >= 'a' && c <= 'z')ret = c;
		else ret = c + LOWER_UPPER_OFFSET;

		return ret;
	}

	static int numberVal(char c){
		return toLower(c) - 'a';
	}

	static char charVal(int i){
		return i + 'a';
	}

	static void insertPair(machine *m, char pair[]){
		assert(checkPair(m->pairs,pair)==LEGAL);
		m->tester.transfer[numberVal(pair[0])] = pair[1];
		m->tester.transfer[numberVal(pair[1])] = pair[0];
		int i = numberVal(pair[0]);
		int j = numberVal(pair[1]);


			m->pairs[i][j] = SET;
			m->pairs[j][i] = SET;

	}

	static machine starter(){
		machine ret;
		int i = 0,j;
		while(i<NUM_ROTORS){
			ret.turn[i] = 0;
			ret.rotorID[i] = 0;
			i++;
		}
		ret.rotorID[REFLECTOR] = 4;
		i=0;
		while(i<NUM_ELEMS){
			ret.tester.transfer[i] = charVal(i);
			i++;
		}

		i=0;
		while(i<NUM_ELEMS){
			j=0;
			while(j<NUM_ELEMS){
				ret.pairs[i][j] = UNSET;
				j++;
			}
			i++;
		}

		return ret;
	}

	static int checkPair(int pairs[NUM_ELEMS][NUM_ELEMS], char pair[]){
		if(pair[0] == '{')printf("0\n");
		if(pair[1] == '{')printf("1\n");

		int first = numberVal(pair[0]);
		int secnd = numberVal(pair[1]);
		int ret = LEGAL;
		int i,j,k;
		if(pairs[first][secnd] != UNSET)ret = ILLEGAL;

		i=0;j=secnd;
		while(i<NUM_ELEMS && ret == LEGAL){
			if(pairs[i][j] == SET || pairs[j][i] == SET){
				ret = ILLEGAL;
			}
			i++;
		}i=0;j=first;
		while(i<NUM_ELEMS && ret == LEGAL){
			if(pairs[i][j] == SET || pairs[j][i] == SET){
				ret = ILLEGAL;
			}
			i++;
		}

		i=0;k=0;
		while(i<NUM_ELEMS){
			j=0;
			while(j<NUM_ELEMS){
				if(pairs[i][j] == SET){k++;}
				j++;
			}
			i++;
		}
		if(k == 2*NUM_BOARD_PAIRS)ret = ILLEGAL;
		return ret;
	}

	static void reinitPairs(int pairs[NUM_ELEMS][NUM_ELEMS]){
		int i,j;
		i=0;
		while(i<NUM_ELEMS){
			j=0;
			while(j<NUM_ELEMS){
				pairs[i][j] = UNSET;
				j++;
			}
			i++;
		}
	}

	static void eliminate(int pairs[NUM_ELEMS][NUM_ELEMS]){
		int i,j;
		i=0;
		while(i<NUM_ELEMS){
			j=0;
			while(j<NUM_ELEMS){
				if(pairs[i][j] == SET){
					pairs[i][j] = ELIMINATED;
				}
				j++;
			}
			i++;
		}
	}

	static void transferBoard(switchBoard b, char ret[]){
		int i = 0,j;
		int setPairs[NUM_ELEMS];
		i=0;
		while(i<NUM_ELEMS){
			setPairs[i] = UNSET;
			i++;
		} 
		i=0;j=0;
		while(i<NUM_ELEMS){
			if(b.transfer[i] != charVal(i) && setPairs[i] == UNSET){
				assert(j != 2*NUM_BOARD_PAIRS);
				ret[j++] = charVal(i);
				ret[j++] = b.transfer[i];

				setPairs[i] = SET;
				setPairs[numberVal(b.transfer[i])] = SET;
			}
			i++;
		}
		if(j != 2*NUM_BOARD_PAIRS){
			i=0;
			while(i<NUM_ELEMS && j < 2*NUM_BOARD_PAIRS){
				if(setPairs[i] == UNSET){
					ret[j++] = charVal(i);
					ret[j++] = charVal(i);
				}
				i++;
			}
		}
		assert(j == 2*NUM_BOARD_PAIRS);
		ret[j] = '\0';
	}

	static Enigma newTestMachine(machine* m,char* file){
		char pair[] = EMPTY_BOARD;
		Enigma ret = newMachine(m->rotorID,m->turn,pair,file);
		strcpy(ret->board.transfer,m->tester.transfer);
		
		return ret;
	}

	static void nextMachine(machine *m){
		reinitPairs(m->pairs);
		int i=0;
		while(i<NUM_ELEMS){
			m->tester.transfer[i] = charVal(i);
			i++;
		}

		i=0;
		while(i<NUM_ROTORS && m->turn[i] == NUM_ELEMS-1){
			m->turn[i] = 0;
			i++;
		}
		if(i == NUM_ROTORS){
			i=0;
			while(i<NUM_ROTORS-1 && m->rotorID[i] == 3){//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<4 is reflector
				m->rotorID[i] = 0;
				i++;
			}
			if(i == NUM_ROTORS-1)m = NULL;
			else {
				m->rotorID[i]++;
			}
		}
		else {
			m->turn[i] ++;
		}
	}

//solver
	static machine solver(char* codeText,char* plainText){

		machine ret = starter();
		machine* bombe = &ret;
		int coord[2],state;
		int cursor = 0;
		assert(strlen(codeText) == strlen(plainText));
		char startPair = 'a';
		char pairTmp[3];
		pairTmp[2] = '\0';

		char pass;
		int loopControl = FALSE;

		Enigma tester;
		while(codeText[cursor] != '\0'){
			printf("%4d",cursor);
			printMachine(bombe);
			startPair = 'a';
			tester = newTestMachine(bombe,TEST_ROTOR);
			
			pairTmp[0] = codeText[cursor];
			
			do{
				pairTmp[1] = startPair;
				if(checkPair(bombe->pairs,pairTmp)==LEGAL){loopControl = FALSE;insertPair(bombe,pairTmp);}
				else{
					coord[0] = numberVal(pairTmp[0]);
					coord[1] = numberVal(pairTmp[1]);
					state = bombe->pairs[coord[0]][coord[1]];
					if(state == SET){
						loopControl = FALSE;
					}
					else{
						loopControl = TRUE;
					}
				}
				startPair++;
			}while(loopControl == TRUE && startPair <= 'z');
			

			if(startPair == '{'){
				startPair = 'a';	
				cursor = -1;//for the coming cursor++;
				nextMachine(bombe);
				if(bombe == NULL){
					printf("Solution not found\nEXITING...\n");
					destroyMachine(tester);
					assert(0);
				}
			}
			else {
				
				destroyMachine(tester);
				tester = newTestMachine(bombe,TEST_ROTOR);
				pass = passBoard(tester,codeText[cursor]);
				pass = passRotors(tester,pass);
				pairTmp[0] = pass;
				pairTmp[1] = plainText[cursor];
				if(checkPair(bombe->pairs,pairTmp) == ILLEGAL){
					
					coord[0] = numberVal(pairTmp[0]);
					coord[1] = numberVal(pairTmp[1]);
					state = bombe->pairs[coord[0]][coord[1]];
					if(state == SET){
						pass = passBoard(tester,pass);
						assert(pass == plainText[cursor]);
					}
					else{
						cursor = -1;
						eliminate(bombe->pairs);
					}
				}
				else {
					insertPair(bombe,pairTmp);
					destroyMachine(tester);
					tester = newTestMachine(bombe,TEST_ROTOR);
					pass = passBoard(tester,pass);
					if(!(pass == plainText[cursor])){
						printf("<%c,%c><%d>\n",pass,plainText[cursor],cursor);
						assert(0);
					}	
				}
			}

			destroyMachine(tester);
			cursor++;
		}

		return ret;
	}

	static void printMachine(machine* m){
		printf("<%d><%d><%d> ",m->rotorID[0],m->rotorID[1],m->rotorID[2]);
		printf(" <%d><%d><%d>\n",m->turn[0],m->turn[1],m->turn[2]);
		
	}
